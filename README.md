A hybrid mobile application developed using the Ionic Framework, Firebase, and D3.js library for iOS, Android, and
Microsoft devices. Primary purpose of the application is to give existing internet banking customers a graphical overview of the
transactions they have committed with line charts, graph, and pie charts.

![Alt Text](https://media.giphy.com/media/1zhpePOrHIlglLsnAd/giphy.gif)