import { Component } from '@angular/core';
import { Platform, ionicBootstrap } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { LandingPage } from './pages/landing/landing';
//import { LoginPage } from './pages/login/login';
//import { HomePage } from './pages/home/home';
//import * as firebase from 'firebase';

@Component({
  template: '<ion-nav [root]="rootPage" swipeBackEnabled="false"></ion-nav>',
})
export class MyApp {

  public rootPage: any;

  constructor(private platform: Platform) {
    this.rootPage = LandingPage;

//     const fbConf = {
//       apiKey: "AIzaSyBMohjcflrHj86pxcUQxh4yS76XhD1A5c8",
//     	authDomain: "myapp-50571.firebaseapp.com",
//     	databaseURL: "https://myapp-50571.firebaseio.com",
//     	storageBucket: "myapp-50571.appspot.com"
//     };
//     firebase.initializeApp(fbConf);

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.overlaysWebView(true);
      StatusBar.backgroundColorByHexString("#2A0845");
    });
  }
}

ionicBootstrap(MyApp);
