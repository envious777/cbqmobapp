import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Authservice provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Authservice {
isLoggedin: any;
AuthToken: any;

  constructor(private http: Http) {
    this.isLoggedin = false;
    this.AuthToken = null;
  }

  storeUserCredentials(token){
    window.localStorage.setItem('apptoken', token);
    this.useCredentials(token);
  }

  useCredentials(token){
    this.isLoggedin = true;
    this.AuthToken = token;
  }

  loadCredentials(){
    let token = window.localStorage.getItem('apptoken');
    this.useCredentials(token);
  }

  destroyUserCredentials(){
    this.isLoggedin = false;
    this.AuthToken = null;
    window.localStorage.clear();
  }

  authenticate(user){
    var creds = "username="+ user.username + "&password=" + user.password;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
// http://localhost:8080/authenticate
// http://localhost:8080/getdata
    return new Promise(resolve => {
      this.http.post('https://cbqdash.herokuapp.com/authenticate', creds, {headers: headers}).subscribe(data => {
        if(data.json().success){
            this.storeUserCredentials(data.json().token);
            resolve(true);
        }
        else{
          resolve(false);
        }
      });
    });
  }

  getData(){
     return new Promise(resolve => {
       var headers = new Headers();
       this.loadCredentials();
       headers.append('Authorization', 'Bearer ' + this.AuthToken);
       this.http.get('https://cbqdash.herokuapp.com/getdata', {headers: headers}).subscribe(data => {
          if(data.json().success){
            resolve(data.json());
          }
          else
            resolve(false);
       });
     })
  }

  logout(){
    this.destroyUserCredentials();
  }
}
