import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as firebase from 'firebase';

/*
  Generated class for the TransactionProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TransactionProvider {
  db: any;
  auth: any;
  constructor(private http: Http) {
    this.db = firebase.database().ref('/');
    this.auth = firebase.auth();
  }
}
