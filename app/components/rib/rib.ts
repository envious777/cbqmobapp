import { Component, Input, Output, EventEmitter} from '@angular/core';
import * as c3 from 'c3';
import * as d3 from 'd3';
import * as _ from 'lodash';

@Component({
  selector: 'rib',
  templateUrl: 'build/components/rib/rib.html'
})
export class Rib {

  chart: any;
  public credsum: any;
  public debsum: any;

  @Input() trans : any;
  @Output() dcSum = new EventEmitter();

  constructor(){
   this.loadChart()
  }

  load(){
    this.credsum = _.sum(_.map(_.filter(this.trans.RIB, ["Type","Credit"]),'Amount'));
    this.debsum = _.sum(_.map(_.filter(this.trans.RIB, ["Type","Debit"]),'Amount'));
  }

  thisChartLoad(){
    this.chart.load({
        columns: [
            ["debit", this.debsum],
            ["credit", this.credsum],
        ]
    });
  }

  emitSum(){
    this.dcSum.emit({
      debit: this.debsum,
      credit: this.credsum
    });
  }

  loadChart(){
    window.setTimeout(() => {
      this.load();
      this.chart = c3.generate({
          padding: {
            top: 0.1
          },
          data: {
                columns: [
                    ['debit', 0],
                    ['credit', 0],
                ],
                type : 'donut',
                colors: {
                   debit: "#e53935",
                   credit:  "#39b88c"
                }
                // onclick: function (d, i) { console.log("onclick", d, i); },
                // onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                // onmouseout: function (d, i) { console.log("onmouseout", d, i); }
          },
          donut: {
              title: "Retail",
          },
          tooltip: {
              format: {
                  value: function (value, ratio, id) {
                      return d3.format('s')(value);
              }
            }
          }
        });

        setTimeout(() => {
          this.thisChartLoad();
        },500);

        this.emitSum();

    },1000);
  }

  reloadChart(currency){
    if(_.isUndefined(currency)){
      this.load();
      this.thisChartLoad();
      this.emitSum();
    }
    else{
      var currsumObj = _.map(_.groupBy(this.trans.RIB,'Currency'), function(key, value){
            return{
              Debit: _.sum(_.map(_.filter(key, ['Type','Debit']),'Amount')),
              Credit: _.sum(_.map(_.filter(key, ['Type','Credit']),'Amount')),
              Currency: value
            }
          });
          //console.log(currsumObj);
          this.credsum = _.map(_.filter(currsumObj,{'Currency':currency}),'Credit');
          this.credsum = this.credsum[0];
          //console.log(this.credsum);
          this.debsum = _.map(_.filter(currsumObj,{'Currency':currency}),'Debit');
          this.debsum = this.debsum[0];

          if(this.debsum === undefined){
            this.debsum = 0;
          }
          if(this.credsum === undefined){
            this.credsum = 0;
          }

          this.thisChartLoad();

          this.emitSum();
    }
  }

  // loadTrans(){
  //   this.tp.load()
  //     .then(data => {
  //       this.trans = data;
  //       this.loadChart();
  //     });
  // }
}
