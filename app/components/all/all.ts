//import {IONIC_DIRECTIVES} from 'ionic-angular';
import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import * as c3 from 'c3';
import * as d3 from 'd3';
import * as _ from 'lodash';
@Component({
  selector: 'all',
  templateUrl: 'build/components/all/all.html'
})
export class All{
  chart: any;
  data: any;
  debitAmt: any;
  debitAmts: any;
  credAmt: any;
  credAmts: any;
  date: any;
  dates: any;
  newObj: any;
  fdate: any;
  public debitsum: any;
  public creditsum: any;
  d:any;

  @Input() trans : any;
  @Output() dcSum = new EventEmitter();

  constructor(){
    this.loadChart();
  }

  load(){
    this.data = _.union(this.trans.CIB,this.trans.RIB);

    var newObj = _.map(_.groupBy(_.flatten(this.data),'Date'), function(key, value){
      return{
        Debit: _.sum(_.map(_.filter(key, ['Type','Debit']),'Amount')),
        Credit: _.sum(_.map(_.filter(key, ['Type','Credit']),'Amount')),
        Date: value
      }
    });

    this.debcredSum(newObj);

    this.debitAmtsFormat();
    this.creditAmtsFormat();

    this.date = _.map(newObj, 'Date');
    this.dates = ["x"];
    this.dates.push(this.date);
    this.dates = _.flatten(this.dates);

    this.fdate = _.pullAt(this.date,[1,3,5]);
  }


  loadChart(){
    window.setTimeout(() => {
      this.load();
      this.chart = c3.generate({
          bindto: "#chart",
          data: {
              x : 'x',
              columns: [
                  this.dates,
                  ["debit", 0,0,0,0,0,0,0],
                  ["credit", 0,0,0,0,0,0,0],
              ],
              types: {
                  debit: 'spline',
                  credit: 'spline'
              },
              colors: {
                debit: "#e53935",
                credit:  "#39b88c"
              },
          },
          point: {
            r: 3,
            focus: {
              expand: {
                r: 4
              }
            }
          },
          tooltip: {
              grouped: false,
          },
          axis: {
                  x : {
                          type : 'timeseries',
                          tick: {
                              values: this.fdate,
                              format: "%e %b"
                          }
                      },
                  y : {
                          tick: {
                              format: d3.format("s")
                          }
                      }
          },
          padding: {
            top:0,
            bottom:0,
            right: 0,
            left: 50
          },
          transition: {
              duration: 1000
          }
      });

      d3.select('.c3-axis.c3-axis-x').attr('clip-path',"null");

      this.thisChartLoad();

      this.emitSum();

    },1000);
  }

  debcredSum(Obj){
    this.debitAmt = _.map(Obj, 'Debit');
    this.debitsum = _.sum(this.debitAmt);
    this.credAmt = _.map(Obj, 'Credit');
    this.creditsum = _.sum(this.credAmt);
  }

  debitAmtsFormat(){
    this.debitAmts = ["debit"];
    this.debitAmts.push(this.debitAmt);
    this.debitAmts = _.flatten(this.debitAmts);
    for(let i=0; i < this.debitAmts.length; i++){
      if(this.debitAmts[i] === undefined){
        this.debitAmts[i] = 0;
      }
    }
  }

  creditAmtsFormat(){
    this.credAmts = ["credit"];
    this.credAmts.push(this.credAmt);
    this.credAmts = _.flatten(this.credAmts);
    for(let i=0; i < this.credAmts.length; i++){
      if(this.credAmts[i] === undefined){
        this.credAmts[i] = 0;
      }
    }
  }

  thisChartLoad(){
    this.chart.load({
              columns: [
                this.debitAmts,
                this.credAmts
              ]
    });
  }

  emitSum(){
    this.dcSum.emit({
      debit: this.debitsum,
      credit: this.creditsum
    });
  }

  reloadChart(currency){
      if(_.isUndefined(currency)){
        this.load();
        this.thisChartLoad();
        this.emitSum();
      }
      else{
        var linechartObj = _.map(_.groupBy(_.flatten(_.union(this.trans.CIB,this.trans.RIB)),'Date'), function(key, value){
          return{
            Debit: _.sum(_.map(_.filter(key, {'Type':'Debit', 'Currency': currency}),'Amount')),
            Credit: _.sum(_.map(_.filter(key, {'Type':'Credit', 'Currency': currency}),'Amount')),
            Date: value
          }
        });

        this.debcredSum(linechartObj);
        this.debitAmtsFormat();
        this.creditAmtsFormat();

        this.thisChartLoad();

        this.emitSum();
      }
    }

}
