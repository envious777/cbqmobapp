import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { LoginPage } from '../login/login';
// import * as svgjs from 'svg.js';

@Component({
  templateUrl: 'build/pages/landing/landing.html'
})

export class LandingPage {
  today: number = Date.now();

  // refreshDuration = 7500;
  // refreshTimeout;
  // numPointsX;
  // numPointsY;
  // unitWidth;
  // unitHeight;
  // points;

  constructor(private navCtrl: NavController, private _platform: Platform) {
    // this.onLoad();

    }

  navToLogin() {
    this.navCtrl.push(LoginPage);
  }
}
