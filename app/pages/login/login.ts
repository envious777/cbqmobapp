import { Component } from '@angular/core';
import { NavController, Platform, ToastController } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { AndroidFingerprintAuth, TouchID } from 'ionic-native';
import { HomePage } from '../home/home';
import * as firebase from 'firebase';
//import { TransactionProvider } from './../../providers/transaction-provider/transaction-provider';
import { Authservice } from './../../providers/authservice/authservice';

@Component({
  templateUrl: 'build/pages/login/login.html',
  providers: [Authservice]
})

export class LoginPage {
  touchIdAvailable: boolean;
  androidTouchAvailable: boolean;
//  users: any;
  user: any;

  constructor(private navCtrl: NavController, private _platform: Platform, private fb: FormBuilder,
    public toast: ToastController,private auth: Authservice) {

      this._platform.ready().then(() => {
      AndroidFingerprintAuth.isAvailable()
        .then((result)=> {
          if(result.isAvailable){
            // it is available
            this.androidTouchAvailable = true;
          } else {
            console.log("Fingerprint Not Available");
            // fingerprint auth isn't available
            this.androidTouchAvailable = false;
          }
        })
        .catch(error => console.error(error));
        });

        this._platform.ready().then(() => {
          TouchID.isAvailable().then(
            res => this.touchIdAvailable = true,
            err => this.touchIdAvailable = false
          );
        });
    }

    presentToast(){
      let toast = this.toast.create({
         message: 'Welcome to your Dashboard!',
         duration: 3000,
         cssClass: "toast"
       });
       toast.present();
    }

  // ionViewWillEnter(){
  //   this.data.db.child('users').on('value', data =>{
  //     this.users = data.val();
  //     console.log(this.users);
  //   })
  // }

  ionViewLoaded(){
    this.user = this.fb.group({
      username:['',Validators.required],
      password: ['', Validators.required]
    });
  }

  signIn(){
    this.auth.authenticate(this.user.value).then(data => {
      if(data) {
         this.navCtrl.push(HomePage);
         this.presentToast();
      }
      else if(data===false) {
        let toast = this.toast.create({
           message: 'Oops! Username/Password Entered Incorrectly',
           duration: 3000,
           cssClass: "toast"
         });
        toast.present();
      }
    });
  }

  appletouch(){
      TouchID.verifyFingerprint('Scan your Fingerprint Please')
        .then(
          res => {
            console.log('Ok', res);
            this.presentToast();
            this.navCtrl.push(HomePage);
          },
          err => console.error('Oops! Didn\'t Authenticate', err)
        );
  }

  androidtouch(){
    AndroidFingerprintAuth.show({ clientId: "myAppName", clientSecret: "so_encrypted_much_secure_very_secret", disableBackup: true })
      .then(result => {
         if(result.withFingerprint) {
           console.log('Successfully authenticated with fingerprint!');
           this.presentToast();
           this.navCtrl.push(HomePage);
         } else
            console.log('Oops! Didn\'t Authenticate');
      })
      .catch(error => console.error(error));
  }


}
