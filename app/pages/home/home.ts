import { Component, Input, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Authservice } from './../../providers/authservice/authservice';
import { LandingPage } from '../landing/landing';
import * as _ from 'lodash';

import { All } from './../../components/all/all';
import { Cib } from './../../components/cib/cib';
import { Rib } from './../../components/rib/rib';

@Component({
  templateUrl: 'build/pages/home/home.html',
  directives: [All,Cib,Rib],
  providers: [Authservice]
})

export class HomePage {
  @ViewChild(All) a: All;
  @ViewChild(Rib) r: Rib;
  @ViewChild(Cib) ci: Cib;

  isAll: boolean = true;
  isRib: boolean = false;
  isCib: boolean = false;
  yshow: boolean = true;
  chart: any;
  curs = ['QAR','USD','EUR','GBP','INR'];
  c: any;
  today: number = Date.now();
  totaldebit: any;
  totalcredit: any;
  d: any;
  name: any;
  transactions: any;

  constructor(private navCtrl: NavController, private alert: AlertController, private auth: Authservice) {
    this.auth.getData().then(data => {
      this.d = data;
      this.name = this.d.name;
      this.transactions = this.d.transactions;
    });
  }

  logoutAlert() {
   let confirm = this.alert.create({
     title: 'Do you wish to Logout?',
     buttons: [
       {
         text: 'No',
         handler: () => {
          // console.log('No clicked');
         }
       },
       {
         text: 'Yes',
         handler: () => {
          // console.log('Yes clicked');
            this.auth.logout();
            this.navCtrl.pop(HomePage);
            this.navCtrl.push(LandingPage);
         }
       }
     ]
   });
   confirm.present();
 }

  all(){
    //document.getElementById("curitems").classList.remove("item-radio-checked");
    this.isAll = true;
    this.isRib = false;
    this.isCib = false;
    this.yshow = true;
  }

  rib(){
    this.isAll = false;
    this.isRib = true;
    this.isCib = false;
    this.yshow = false;
  }

  cib(){
    this.isAll = false;
    this.isRib = false;
    this.isCib = true;
    this.yshow = false;
  }

  dcamount(event){
  //  console.log(event);
    this.totaldebit = event.debit;
    this.totalcredit = event.credit;
  }

  checked(cur){
    if(this.isAll){
      this.a.reloadChart(cur);
    }
    if(this.isRib){
      this.r.reloadChart(cur);
    }
    if(this.isCib){
      this.ci.reloadChart(cur);
    }
  }


}
